using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace UmbracoV8.Core
{
    public class HomeController : RenderMvcController
    {
        private static int visitorCount = 0;

        public ActionResult Index(ContentModel model)
        {
            Logger.Info<HomeController>($"PublishedRequest?.Domain?.Uri?.Host: {PublishedRequest?.Domain?.Uri?.Host} and HTTP_UC_REQUESTED_HOST: {Request.ServerVariables.GetValues("HTTP_UC_REQUESTED_HOST").FirstOrDefault()}\n{model.Content.Id} $$ {Request.Url} ## {Guid.NewGuid()} visit: {++visitorCount}");
            return CurrentTemplate(model);
        }
    }
}
